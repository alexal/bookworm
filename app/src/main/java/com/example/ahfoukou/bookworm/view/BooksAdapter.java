package com.example.ahfoukou.bookworm.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ahfoukou.bookworm.R;
import com.example.ahfoukou.bookworm.domain.Book;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahfoukou on 18/4/2018.
 */

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.BookHolder>{

    private final List<Book> bookList;
    private final EditBookListener editBookListener;
    private final DeleteBookListener deleteBookListener;

    public BooksAdapter(List<Book> bookList,
                        @NonNull EditBookListener editBookListener,
                        @NonNull DeleteBookListener deleteBookListener) {

        this.bookList = new ArrayList<>(bookList);
        this.editBookListener = editBookListener;
        this.deleteBookListener = deleteBookListener;
    }

    @Override
    public BookHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_book_item,
                                                                     parent,false);

        return new BookHolder(view);
    }

    @Override
    public void onBindViewHolder(final BookHolder holder, final int position) {

        holder.bookTitle.setText(
                bookList.get(position).getTitle());

        holder.publisherName.setText(
                bookList.get(position).getPublisher());

        holder.rating.setText(
                String.valueOf(bookList.get(position).getRating()));

        holder.editBookView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editBookListener.onEditBook(
                        bookList.get(position).getId());
            }
        });

        holder.deleteBookView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteBookListener.onDeleteBook(
                        bookList.get(position).getId()
                );
            }
        });

    }

    public void updateBookList(@NonNull List<Book> newBookList){

        bookList.clear();
        bookList.addAll(newBookList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    public interface DeleteBookListener {
        void onDeleteBook(String id);

    }

    public interface EditBookListener {
        void onEditBook(String id);

    }

    static class BookHolder extends RecyclerView.ViewHolder{

        TextView bookTitle;
        TextView publisherName;
        TextView rating;
        ImageView editBookView;
        ImageView deleteBookView;

        BookHolder(View itemView) {
            super(itemView);

            bookTitle       = itemView.findViewById(R.id.tv_book_item_title);
            publisherName   = itemView.findViewById(R.id.tv_book_item_publisher);
            rating          = itemView.findViewById(R.id.tv_book_item_rating);
            editBookView    = itemView.findViewById(R.id.iv_book_item_edit);
            deleteBookView  = itemView.findViewById(R.id.iv_book_item_delete);
        }
    }
}
