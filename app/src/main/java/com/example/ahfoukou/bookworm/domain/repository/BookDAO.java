package com.example.ahfoukou.bookworm.domain.repository;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.ahfoukou.bookworm.domain.Book;

import java.util.List;

/**
 * Created by ahfoukou on 18/4/2018.
 */
@Dao
public interface BookDAO {

    @Insert
    void addBook(Book book);

    @Update
    void updateBook(Book book);

    @Delete
    void deleteBook(Book book);

    @Query("SELECT * FROM book WHERE id LIKE :id")
    Book getById(String id);

    @Query("SELECT * FROM book")
    List<Book> getAllBooks();

    @Query("SELECT * FROM book ORDER BY rating DESC LIMIT 3")
    List<Book> getTop3RatedBooks();

    @Query("SELECT * FROM book ORDER BY rating ASC LIMIT 3")
    List<Book> getLowest3RatedBooks();

}
