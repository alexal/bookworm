package com.example.ahfoukou.bookworm.domain.repository;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;

import com.example.ahfoukou.bookworm.domain.Book;

import java.util.List;

public class BookRepository {

    private BookDAO bookDAO;

    public BookRepository(Context context){
        BookDatabase bookDatabase = BookDatabase.getInstance(context);
        bookDAO = bookDatabase.bookDAO();
    }

    /**
     * Adds the book to the database.
     * @param book the book to be added
     * @return true if the book does not exist and is added, otherwise
     * false if duplicate.
     */
    public boolean addBook(Book book){
        try {
            bookDAO.addBook(book);
            return true;
        }
        catch (SQLiteConstraintException exception){
            bookDAO.updateBook(book);
            return false;
        }

    }

    public void deleteBook(Book book){
        bookDAO.deleteBook(book);
    }

    public void updateBook(Book book){
        bookDAO.updateBook(book);
    }

    public List<Book> getAllBooks(){
        return bookDAO.getAllBooks();
    }

    public List<Book> getTop3RatedBooks(){
        return bookDAO.getTop3RatedBooks();
    }

    public List<Book> getLowest3RatedBooks(){
        return bookDAO.getLowest3RatedBooks();
    }

    public Book getById(String id){
        return bookDAO.getById(id);
    }

    public interface TransactionCallback {

        void onQueryFinished();
    }
}
