package com.example.ahfoukou.bookworm.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.example.ahfoukou.bookworm.R;

public class BookActionsDialogFragment extends DialogFragment {

    private static final String DIALOG_TITLE_KEY = "title_key";
    private static final String BOOK_TITLE_KEY = "book_title_key";
    private static final String BOOK_PUBLISHER_KEY = "book_publisher_key";
    private static final String BOOK_RATING_KEY = "book_rating_key";

    public static BookActionsDialogFragment newInstance(String dialogTitle,
                                                        String bookTitle,
                                                        String publisherTitle,
                                                        int ratingNum) {
        Bundle args = new Bundle();
        args.putString(DIALOG_TITLE_KEY,dialogTitle);
        args.putString(BOOK_TITLE_KEY,bookTitle);
        args.putString(BOOK_PUBLISHER_KEY,publisherTitle);
        args.putInt(BOOK_RATING_KEY,ratingNum);

        BookActionsDialogFragment fragment = new BookActionsDialogFragment();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        renderInitial(view,getArguments());
}

    private void renderInitial(View root,Bundle args) {

        setCancelable(false);

        final String dialogTitle = args.getString(DIALOG_TITLE_KEY);
        final String bookTitle = args.getString(BOOK_TITLE_KEY);
        final String publisherName = args.getString(BOOK_PUBLISHER_KEY);
        final int ratingNum = args.getInt(BOOK_RATING_KEY);

        final EditText bookTitleEditView = root.findViewById(R.id.edtx_book_title);
        final EditText bookPublisherEditView = root.findViewById(R.id.edtx_book_publisher);
        final RatingBar ratingBar = root.findViewById(R.id.ratingBar_book);
        final Button saveButton = root.findViewById(R.id.btn_book_save);

        getDialog().setTitle(dialogTitle);

        bookTitleEditView.setText(bookTitle);
        bookPublisherEditView.setText(publisherName);
        ratingBar.setRating(ratingNum);

        //Set the listener for the save button
        try {

            //We suppose that the activity handling this fragment implements the listener
            final SaveButtonListener saveButtonListener = (SaveButtonListener) getActivity();
            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (bookTitleEditView.getText().toString().isEmpty() || bookPublisherEditView.getText().toString().isEmpty() || ((int) ratingBar.getRating() == 0)){

                        Toast.makeText(getContext(), getString(R.string.toast_book_credentials), Toast.LENGTH_SHORT).show();
                    }
                    else {
                        saveButtonListener.onSaveBookClick(
                                bookTitleEditView.getText().toString(),
                                bookPublisherEditView.getText().toString(),
                                (int) ratingBar.getRating());
                        //The dialog closes itself
                        dismiss();
                    }

                }
            });
        }

        catch (ClassCastException e){
            Log.e("BookActionDialogFragmt","You must implement saveButtonListener duuuude");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_edit_book, container, false);
    }

    public interface SaveButtonListener {
        void onSaveBookClick(String bookTitle, String publisherName, int rating);
    }


}
