package com.example.ahfoukou.bookworm.domain.repository;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.ahfoukou.bookworm.domain.Book;

@Database(entities = Book.class, version = 1)
abstract class BookDatabase extends RoomDatabase{

    private static BookDatabase INSTANCE;

    private static final String DB_NAME = "books.db";

    abstract BookDAO bookDAO();

    static BookDatabase getInstance(Context context){

        if (INSTANCE == null) {
            synchronized (BookDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = buildDatabase(context);
                }
            }
        }
        return INSTANCE;
    }

    private static BookDatabase buildDatabase(final Context context){

        return Room.databaseBuilder(context,
                BookDatabase.class,DB_NAME)
                .allowMainThreadQueries()
                .build();

    }
}
