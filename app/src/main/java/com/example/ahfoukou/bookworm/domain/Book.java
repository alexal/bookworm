package com.example.ahfoukou.bookworm.domain;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

/**
 * Created by ahfoukou on 18/4/2018.
 */
@Entity(primaryKeys = {"title","publisher"})
public class Book {

    private String id;

    @ColumnInfo(name = "title")
    @NonNull
    private String title;

    @ColumnInfo(name = "publisher")
    @NonNull
    private String publisher;

    @ColumnInfo(name = "rating")
    private int rating;

    public Book(@NonNull String title,
                @NonNull String publisher,
                int rating) {

        this.id = title + publisher;
        this.title = title;
        this.publisher = publisher;
        this.rating = rating;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(@NonNull String title) {
        this.title = title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(@NonNull String publisher) {
        this.publisher = publisher;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }


}
