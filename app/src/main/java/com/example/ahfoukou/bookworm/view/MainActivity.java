package com.example.ahfoukou.bookworm.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.ahfoukou.bookworm.R;
import com.example.ahfoukou.bookworm.domain.repository.BookRepository;
import com.example.ahfoukou.bookworm.domain.Book;

import java.util.List;

public class MainActivity
        extends AppCompatActivity
        implements BooksAdapter.DeleteBookListener,
        BooksAdapter.EditBookListener,
        BookActionsDialogFragment.SaveButtonListener{

	private static final String TAG = "MainActivity";
	private static final int FILTER_OPTION_ALL = 0;
	private static final int FILTER_OPTION_TOP = 1;
	private static final int FILTER_OPTION_LOWEST = 2;
    private static final String CUR_FILTER_OPTION_KEY = "FILTER_KEY";

	private FloatingActionButton addButton;
    private RecyclerView booksRecyclerView;
    private TextView listTitleView;
    private Toolbar toolbar;
    private BooksAdapter booksAdapter;

	private BookRepository bookRepository;
	private int currentFilterOption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        initIds();
        initToolbar();
        if (savedInstanceState == null)
            initBookList(FILTER_OPTION_ALL);
        else
            initBookList(savedInstanceState.getInt(CUR_FILTER_OPTION_KEY));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CUR_FILTER_OPTION_KEY,currentFilterOption);
    }

    private void initBookList(int filterOption) {
        //Init book repo
        bookRepository = new BookRepository(this);

        //At initialization no filter is applied
	    currentFilterOption = FILTER_OPTION_ALL;
		listTitleView.setText(getFilteredBooksTitle(currentFilterOption));

        //Get all books from repository
        final List<Book> allBooks = getFilteredBooks(filterOption);

        booksAdapter = new BooksAdapter(allBooks, this, this);

        final RecyclerView.LayoutManager llm = new LinearLayoutManager(this,
                                            LinearLayoutManager.VERTICAL,false);
        booksRecyclerView.setLayoutManager(llm);
        booksRecyclerView.addItemDecoration(new DividerItemDecoration(this,
                                                                      DividerItemDecoration.VERTICAL));
        booksRecyclerView.setAdapter(booksAdapter);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,
                menu);
        return true;
    }

    private void initIds() {
        addButton = findViewById(R.id.fab_add);
        booksRecyclerView = findViewById(R.id.rv_books);
        toolbar = findViewById(R.id.toolbar_main);
		listTitleView = findViewById(R.id.tv_main_list_title);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAddBookClick();
            }
        });
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

    	switch (item.getItemId()){
		    case R.id.menu_main_show_all:
				currentFilterOption = FILTER_OPTION_ALL;
		    	break;
		    case R.id.menu_main_show_lowest:
		    	currentFilterOption = FILTER_OPTION_LOWEST;
			    break;
		    case R.id.menu_main_show_top:
		    	currentFilterOption = FILTER_OPTION_TOP;
		    	break;
	    }

	    updateBookListUI();
		return true;
	}

	private List<Book> getFilteredBooks(int filterOption){
    	if (filterOption == FILTER_OPTION_ALL)
    		return bookRepository.getAllBooks();
    	else if (filterOption == FILTER_OPTION_TOP)
    		return bookRepository.getTop3RatedBooks();
    	else
    		return bookRepository.getLowest3RatedBooks();
	}

	private String getFilteredBooksTitle(int filterOption){
		if (filterOption == FILTER_OPTION_ALL)
			return getString(R.string.all_books);
		else if (filterOption == FILTER_OPTION_TOP)
			return getString(R.string.top3);
		else
			return getString(R.string.lowest3);
	}

	/**
	 * Updates the books list shown in the Ui based on the current filter option
	 */
	private void updateBookListUI(){
		//Update list title
		listTitleView.setText(getFilteredBooksTitle(currentFilterOption));
		//Update book entries
		final List<Book> booksToDisplay = getFilteredBooks(currentFilterOption);
		this.booksAdapter.updateBookList(booksToDisplay);
	}

	@Override
    public void onDeleteBook(String id) {
        //Query for the book by its id
        final Book bookToDelete = bookRepository.getById(id);
        //Remove from repository
        bookRepository.deleteBook(bookToDelete);
        //Update UI
        this.updateBookListUI();
    }
    @Override
    public void onEditBook(String id) {
        //Get the book to edit by its id
        final Book bookToEdit = bookRepository.getById(id);

        final BookActionsDialogFragment editFragment = BookActionsDialogFragment.newInstance(
                getString(R.string.book_edit),
                bookToEdit.getTitle(),
                bookToEdit.getPublisher(),
                bookToEdit.getRating());

        getSupportFragmentManager().beginTransaction()
                .add(editFragment, null)
                .commit();

        //HACK HACK
        bookRepository.deleteBook(bookToEdit);
    }

    private void onAddBookClick(){

        //Show a new edit fragment with empty initial values
        final BookActionsDialogFragment addFragment = BookActionsDialogFragment.newInstance(
                getString(R.string.book_add),
                "",
                "",
                0);

        getSupportFragmentManager().beginTransaction()
                .add(addFragment,null)
                .commit();
    }

    @Override
    public void onSaveBookClick(String bookTitle,
                                String publisherName,
                                int rating) {

        final Book book = new Book(bookTitle,
                publisherName,
                rating);

		bookRepository.addBook(book);
        updateBookListUI();
    }
}
